// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

const containerStyle = {
    display: 'block',
    width: '95%'
};

const componentStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    backgroundColor: 'inherit'
};

const labelStyle = {
    display: 'block',
    width: '55%',
    height: '100%',
};

const inputStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    width: '45%',
    backgroundColor: 'inherit',
    marginTop: '3%',
    marginBottom: '3%'
};

const MultiInput = (props) => {
    let onChange = (input) => (
        (e) => {
            let values = [];

            if (props.values && props.values.length) {
                values = [ ...props.values ];
            }

            if (input < values.length) {
                values[input] = e.target.value;
            } else {
                values.push(e.target.value);
            }

            if (props.onChange) {
                props.onChange(values);
            }
        }
    );

    let inputs = [
        (<input type="text" onChange={onChange(props.values ? props.values.length : 0)} />)
    ];

    let index = 0;

    if (props.values && props.values.length) {
        inputs = [
            ...(
                props.values.map(() => {
                    let input = (
                        <input
                            type="text"
                            style={{ marginBottom: '1%' }}
                            onChange={onChange(index)}
                            value={props.values[index]}
                        />);

                    index++;

                    return input;
                })
            ),
            ...inputs
        ];
    }

    return (
        <div style={containerStyle}>
            <div style={props.style ? { ...componentStyle, ...props.style } : componentStyle}>
                <div style={labelStyle}>
                    <h5>{props.label}</h5>
                </div>
                <div style={inputStyle}>
                    {inputs}
                </div>
            </div>
        </div>
    );
};

export default MultiInput;
