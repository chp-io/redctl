// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/redfield/redctl/api"
)

const (
	FileExtSVG = ".svg"
	FileExtPNG = ".png"
)

// GraphicsRepo is used to manage graphics (e.g. desktop icons)
// on the filesystem
type GraphicsRepo struct {
	path string
}

// NewGraphicsRepo returns a new graphics repo with the
// given path
func NewGraphicsRepo(path string) *GraphicsRepo {
	return &GraphicsRepo{path}
}

func (gr *GraphicsRepo) graphicExists(name string, kind api.GraphicType) bool {
	path := gr.absPath(name, kind)
	_, err := os.Stat(path)

	return !os.IsNotExist(err)
}

func (gr *GraphicsRepo) absPath(name string, kind api.GraphicType) string {
	switch kind {
	case api.GraphicType_GRAPHIC_DESKTOP_ICON:
		return filepath.Join(gr.path, "icons", name)
	case api.GraphicType_GRAPHIC_BACKGROUND:
		return filepath.Join(gr.path, "backgrounds", name)
	}

	return filepath.Join(gr.path, name)
}

// ImportDirectory imports all valid files (i.e. .png, .svg) from
// a given directory. In the case of a name conflict, old files
// are over-written by new ones.
//
// If kind is GRAPHIC_UNSPECIFIED, the files from the directory will be imported
// to the base directory of the repository, and will have type GRAPHIC_UNSPECIFIED
// when referenced later.
func (gr *GraphicsRepo) ImportDirectory(path string, kind api.GraphicType) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	for _, file := range files {
		if !isValidGraphicsExtension(file.Name()) {
			continue
		}

		data, err := ioutil.ReadFile(filepath.Join(path, file.Name()))
		if err != nil {
			return err
		}

		err = gr.create(file.Name(), kind, data, true)
		if err != nil {
			return err
		}
	}

	return nil
}

func isValidGraphicsExtension(name string) bool {
	ext := filepath.Ext(name)

	return (ext == FileExtPNG || ext == FileExtSVG)
}

func (gr *GraphicsRepo) create(name string, kind api.GraphicType, data []byte, overwrite bool) error {
	if gr.graphicExists(name, kind) && !overwrite {
		return fmt.Errorf("graphic '%v' exists: overwrite not set", name)
	}

	// Make sure the subdirectory exists.
	if err := os.MkdirAll(gr.absPath("", kind), 0755); err != nil {
		return fmt.Errorf("failed to create graphics subdirectory: %v", err)
	}

	return ioutil.WriteFile(gr.absPath(name, kind), data, 0644)
}

// Imports adds a new graphic to the repo. If kind is GRAPHIC_UNSPECIFIED, the graphic
// will be stored in the base directory of the repository, and will always have type
// GRAPHIC_UNSPECIFIED when referenced later.
func (gr *GraphicsRepo) Import(name string, kind api.GraphicType, data []byte) error {
	if !isValidGraphicsExtension(name) {
		return fmt.Errorf("%v is not of format PNG or SVG", name)
	}

	return gr.create(name, kind, data, false)
}

// Find returns a path to a graphic, if it exists. Otherwise an empty string
// is returned.
func (gr *GraphicsRepo) Find(name string, kind api.GraphicType) string {
	if !gr.graphicExists(name, kind) {
		return ""
	}

	return gr.absPath(name, kind)
}

// Remove removes the graphic from the repository
func (gr *GraphicsRepo) Remove(name string, kind api.GraphicType) error {
	if !gr.graphicExists(name, kind) {
		return fmt.Errorf("graphic '%v' does not exist", name)
	}

	return os.Remove(gr.absPath(name, kind))
}

// FindAll returns all the graphics in the repo of type kind. If kind is set
// to GRAPHIC_UNSPECIFIED, all graphics of all typesd are returned.
func (gr *GraphicsRepo) FindAll(kind api.GraphicType) ([]*api.Graphic, error) {
	graphics := make([]*api.Graphic, 0)

	// The kinds of graphics to find.
	kinds := make([]api.GraphicType, 1)
	kinds[0] = kind

	if kind == api.GraphicType_GRAPHIC_UNSPECIFIED {
		kinds = append(kinds, api.GraphicType_GRAPHIC_DESKTOP_ICON)
		kinds = append(kinds, api.GraphicType_GRAPHIC_BACKGROUND)
	}

	for _, k := range kinds {
		path := gr.absPath("", k)

		files, err := ioutil.ReadDir(path)
		if err != nil {
			return graphics, fmt.Errorf("could not read files from %s: %v", path, err)
		}

		for _, f := range files {
			if !isValidGraphicsExtension(f.Name()) {
				continue
			}

			g := &api.Graphic{
				Name: f.Name(),
				Type: k,
				Path: gr.absPath(f.Name(), k),
			}

			graphics = append(graphics, g)
		}
	}

	return graphics, nil
}
