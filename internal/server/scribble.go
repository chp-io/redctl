// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"fmt"
	"log"
	"os"

	"path/filepath"

	scribble "github.com/nanobox-io/golang-scribble"
)

// ScribbleInitialize initializes database for repos
func ScribbleInitialize(path string) (*scribble.Driver, error) {
	dcPath := filepath.Join(path, "domains")

	err := os.MkdirAll(dcPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", dcPath, err)
		return nil, err
	}

	db, err := scribble.New(path, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create database for redctl: %v", err)
	}

	return db, err
}
